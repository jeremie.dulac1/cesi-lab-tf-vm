terraform {
  required_version = ">=0.12"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.0"
    }
    # random = {
    #   source  = "hashicorp/random"
    #   version = "~>3.0"
    # }
  }
}

provider "azurerm" {
  features {}
  subscription_id = var.provider_authentication["subscription_id"]
  tenant_id       = var.provider_authentication["tenant_id"]
  client_id       = var.provider_authentication["client_id"]
  client_secret   = var.provider_authentication["client_secret"]

  skip_provider_registration = true # This is only required when the User, Service Principal, or Identity running Terraform lacks the permissions to register Azure Resource Providers.
}